package com.vidstaric.demo2

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val SELECT_IMAGE_REQUEST = 123
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageUri = savedInstanceState?.getParcelable("ImageUri")

        setUpView()

        fab.setOnClickListener { _ ->
            if (imageUri == null)
                startGalleryIntent()
            else
                startSendMailIntent()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putParcelable("ImageUri", imageUri)
    }

    private fun startGalleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Please select a gallery app"), SELECT_IMAGE_REQUEST)
    }

   private fun setUpView(){
       showSnackBar()
       setBitmap()
       setFabIcon()
   }

    private fun setFabIcon(){
        if(imageUri != null){
            fab.setImageResource(android.R.drawable.ic_dialog_email)
        } else{
            fab.setImageResource(android.R.drawable.ic_menu_gallery)
        }
    }

    private fun setBitmap() {
        if (imageUri != null) {
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
            image.setImageBitmap(bitmap)
        } else {
            image.setImageResource(R.drawable.ic_launcher_background)
        }
    }

    private fun showSnackBar() {
        if (imageUri != null) {
            Snackbar.make(fab, "Image Selected", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Undo", {
                        imageUri = null
                        setUpView()
                    })
                    .show()
        } else {
            Snackbar.make(fab, "Select a image from the gallery by clicking on the Floating action button", Snackbar.LENGTH_INDEFINITE)
                    .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SELECT_IMAGE_REQUEST) {
            imageUri = data?.data

            setUpView()
        }
    }

    private fun startSendMailIntent() {
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_TEXT, getDeviceData())
        email.type = "message/rfc822"
        email.putExtra(Intent.EXTRA_STREAM, imageUri)
        startActivity(Intent.createChooser(email, "Please select a email client"))
    }

    private fun getDeviceData(): String {
        return "Device{manufacturer=" + Build.MANUFACTURER + ", model=" + Build.MODEL + "}\n" + resources.displayMetrics.toString()
    }
}
